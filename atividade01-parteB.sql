﻿--1. Listar veículos: placa e idade
select	nu_placa,
	date_part('year', CURRENT_DATE) - vl_ano_fabricacao as idade1,
	extract(YEAR from CURRENT_DATE) - vl_ano_fabricacao as idade2
from 	veiculo;

-- 2. Listar veículos: todos os atributos
-- Filtro: fabricados antes de 2015
select	*
from	veiculo
where	vl_ano_fabricacao < 2015;

-- 3. Listar veículos: todos os atributos
-- Filtro: adquiridos antes de 2015 e com quilometragem maior que 40000
select	*
from	veiculo
where	extract(YEAR from dt_aquisicao) < 2015
and	vl_km > 40000;

select	*
from	veiculo
where	dt_aquisicao < '2015-01-01'
and	vl_km > 40000;

-- 4. Listar modelos: todos os atributos
-- Filtro: nome contem a palavra "gol" em qualquer local
select	*
from	modelo
where 	nm like '%gol%';

select	*
from	modelo
where 	nm ilike '%gol%'; -- ilike é um like que ignora diferença entre maiúsculas e minúsculas

-- 5. Listar fabricante:
-- Campos: composição do endereço; na composição de endereço (logradouro todo em
--minúsculo, primeiros 10 caracteres do complemento); nome do fabricante todo em
--maiúsculo;
--Filtro: cnpj com menos de 14 caracteres (lembre-se de remover os caracteres em
--branco do início e fim do campo durante a consulta) ou que possuam caracteres não
--numéricos
select	upper(nm),
	lower((ds_endereco).ds_logradouro) || substr((ds_endereco).ds_complemento,10) as endereco
from	fabricante
where	(length(trim(nu_cnpj)) < 14 or nu_cnpj ~* '[a-z]');
;

-- 6. Listar veículos:
-- Campos (agrupamento): quantidade; quilometragem do mais rodado; quilometragem
-- do menos rodado; quilometragem média; total de quilômetros rodados por todos os
-- veículos
select	count(*) as qtd_total_veiculos,
	max(vl_km) as km_maxima,	
	min(vl_km) as km_minina,
	avg(vl_km) as km_media,
	sum(vl_km) as km_total
from	veiculo;

-- 7. Listar veículos:
-- Campos: ano de fabricação
-- Campos (agrupamento): quantidade; quilometragem do mais rodado; quilometragem
-- do menos rodado; quilometragem média; total de quilometros rodados por todos os
-- veículos
select	vl_ano_fabricacao,
	count(*) as qtd_total_veiculos,
	max(vl_km) as km_maxima,	
	min(vl_km) as km_minina,
	avg(vl_km) as km_media,
	sum(vl_km) as km_total
from	veiculo
group by vl_ano_fabricacao;


-- Insert
-- 8. Insira o grupo 'Básico' e 'Intermediário' e dois valores de diária para o primeiro, um para 01/06/2019 e outro para 01/10/2019
-- Obs: lembre-se de que o cd do grupo deve ter geração automática (serial)
insert into grupo (nm) 
values 
 ('Básico')
,('Intermediário') ;

insert into tabela_precos (cd_grupo, dt_inicio_vigencia, vl_diaria) 
values 
 ((select cd from grupo where nm = 'Básico'), '2019-06-01', 95.99)
,((select cd from grupo where nm = 'Básico'), '2019-10-01', 135.99);

-- 9. Remova a diária com início de vigência em 01/06/2019
delete
from tabela_precos
where dt_inicio_vigencia = '2019-06-01';

-- 10. Atualize todos os nomes de grupo para terem grafia toda em maiúsculo.
update	grupo
set	nm = upper(nm);

-- 11. Selecione todos os preços de diária
-- Campos: nome do grupo, data de início de vigência e valor da diária
select	g.nm,
	tp.dt_inicio_vigencia,
	tp.vl_diaria
from	grupo g
inner join tabela_precos tp on (tp.cd_grupo = g.cd);

select	g.nm,
	tp.dt_inicio_vigencia,
	tp.vl_diaria
from	grupo g,
	tabela_precos tp
where 	tp.cd_grupo = g.cd; -- Funciona, mas não é uma boa prática e vou tirar seus pontos se vc usar!

-- 11. Selecione todos os grupos e seus respectivos preços de diária, grupos que não possuirem 
-- valor de diária devem exibir a mensagem '(sem valor de diária)'
-- Campos: nome do grupo, data de início de vigência e valor da diária
select	g.nm,
	coalesce(cast(tp.dt_inicio_vigencia as char(10)) ,'(sem valor de diária)'),
	coalesce(to_char(tp.dt_inicio_vigencia, 'dd/mm/yyyy') ,'(sem valor de diária)'),
	tp.vl_diaria
from	grupo g
left join tabela_precos tp on (tp.cd_grupo = g.cd);

