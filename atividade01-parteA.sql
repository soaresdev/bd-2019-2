drop type if exists t_endereco cascade;

drop table if exists veiculo cascade;
drop table if exists modelo cascade;
drop table if exists acessorio cascade;
drop table if exists veiculo_acessorio cascade;
drop table if exists grupo cascade;
drop table if exists tabela_precos cascade;
drop table if exists fabricante cascade;

create type t_endereco	as (
	ds_logradouro	varchar(100),
	nu		varchar(10),
	ds_complemento	varchar(100),
	nm_cidade	varchar(40),
	nm_estado	varchar(40));

-- O chassi não está no requisito e colocamos aqui para ilustrar o uso de unique
create table veiculo (
	nu_placa		char(7)		not null,
	nu_chassi		varchar(17)	not null,
	cd_modelo		integer		not null,
	cd_grupo		integer		not null,
	vl_ano_fabricacao	smallint	not null,
	dt_aquisicao		date		not null,
	dt_venda		date		        ,
	vl_km			integer		not null,
	constraint pk_veiculo
		primary key (nu_placa),
	constraint un_veiculo_chassi
		unique (nu_chassi),
	constraint ch_veiculo_km
		check (vl_km >= 0));

create table modelo (
	cd			serial		not null,
	nm			varchar(40)	not null,
	nu_cnpj_fabricante	char(14)	not null,
	constraint pk_modelo
		primary key (cd));

create table acessorio (
	sg			char(3)		not null,
	nm			varchar(20)	not null,
	constraint pk_acessorio
		primary key (sg));


create table veiculo_acessorio (
	nu_placa		char(7)		not null,
	sg_acessorio		char(3)		not null,
	constraint pk_veiculo_acessorio
		primary key (nu_placa, sg_acessorio));

create table grupo (
	cd			serial		not null,
	nm			varchar(20)	not null,
	constraint pk_grupo
		primary key (cd));

create table tabela_precos (
	cd_grupo		integer		not null,
	dt_inicio_vigencia	date		not null,
	vl_diaria		numeric(10,2)	not null,
	constraint pk_tabela_precos
		primary key (cd_grupo, dt_inicio_vigencia),
	constraint ck_tabela_precos_diaria
		check (vl_diaria >= 0));

create table fabricante (
	nu_cnpj			char(14)	not null,
	nm			varchar(40)	not null,
	nu_telefone		char(11)	not null,
	ds_endereco		t_endereco	not null,
	constraint pk_fabricante
		primary key (nu_cnpj));

alter table veiculo
	add constraint fk_veiculo_modelo
		foreign key (cd_modelo)
		references modelo,
	add constraint fk_veiculo_grupo
		foreign key (cd_grupo)
		references grupo;

alter table veiculo_acessorio
	add constraint fk_veiculo_acessorio_veiculo
		foreign key (nu_placa)
		references veiculo
		on delete cascade
		on update cascade,
	add constraint fk_veiculo_acessorio_acessorio
		foreign key (sg_acessorio)
		references acessorio
		on update cascade;

alter table tabela_precos
	add constraint fk_tabela_precos_grupo
		foreign key (cd_grupo)
		references grupo;

alter table modelo
	add constraint fk_modelo_fabricante
		foreign key (nu_cnpj_fabricante)
		references fabricante;
		